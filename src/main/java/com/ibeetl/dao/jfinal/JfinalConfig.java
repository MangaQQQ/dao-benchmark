package com.ibeetl.dao.jfinal;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.template.source.ClassPathSourceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class JfinalConfig {

    @Autowired
    DataSource ds;

    @Bean
    public ActiveRecordPlugin initActiveRecordPlugin() {
        ActiveRecordPlugin arp = new ActiveRecordPlugin(ds);
        arp.setDevMode(false);
        arp.getEngine().setSourceFactory(new ClassPathSourceFactory());
        arp.addMapping("sys_user", JfinalUser.class);
        // 必须手动调用start
        arp.start();
        return arp;
    }
}
