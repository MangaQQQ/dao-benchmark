package com.ibeetl.dao.jfinal;

import com.ibeetl.dao.beetlsql.entity.BeetlSqlUser;
import com.ibeetl.dao.common.TestServiceInterface;
import com.ibeetl.dao.jdbc.UserDao;
import com.ibeetl.dao.mybatis.entity.SysUser;
import com.ibeetl.dao.mybatis.service.MybatisUserService;
import com.jfinal.plugin.activerecord.Db;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.SQLException;

public class JfinalPerformaceTestService implements TestServiceInterface {

    int index = 1;

    @Override
    public void testAdd() {
        new JfinalUser().set("ID",index++).set("CODE","yyx").save();
//        Db.update("insert into sys_user (id,code) values (?,?)",index++,"yyx");
    }

    @Override
    public void testUnique() {
        new JfinalUser().dao().findById(1);
//        Db.find("select * from sys_user where id = 1");
    }

    @Override
    public void testUpdateById() {
        int i =index++;
        Db.update("update sys_user set id=?,code=? where id=?",1,"yyx",i);
    }

    @Override
    public void testPageQuery() {
        new JfinalUser().find("select * from sys_user where code = ? limit ?,? ","yyx",1,10);
    }

    @Override
    public void testExampleQuery() {
        new JfinalUser().findFirst("select * from sys_user where id = ? ",1);
    }

    @Override
    public void testOrmQUery() {
        throw new UnsupportedOperationException();

    }

}
